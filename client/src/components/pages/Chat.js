import React from 'react';
import { Component } from 'react';
import FeedBack from '../feedback/feedback'

class Chat extends Component{


    componentDidMount(){
    // Func(){

        (function(d, m){
        var kommunicateSettings = 
            {"appId":"758ae7ad2d17ab75e7547e29e3a81626","popupWidget":true,"automaticChatOpenOnNavigation":true};
        var s = document.createElement("script"); s.type = "text/javascript"; s.async = true;
        s.src = "https://widget.kommunicate.io/v2/kommunicate.app";
        var h = document.getElementsByTagName("head")[0]; h.appendChild(s);
        window.kommunicate = m; m._globals = kommunicateSettings;
        })(document, window.kommunicate || {});

    }

    render(){
        // {this.Func()}
        return(
            <div>
                {/* <Header/> */}
                <div>
        <FeedBack
				style={{zIndex:'1', position:'fixed', left:'2px!'}}
				position="left"
				numberOfStars={5}
				headerText="Feedback"
				bodyText="Give your valuable feedback"
				buttonText="FEEDBACK"
				handleClose={() => console.log("handleclose")}
				handleSubmit={(data) => 
					fetch('https://formspree.io/f/xqkwpwja', {
						headers: {
							Accept: 'application/json',
							'Content-Type': 'application/json'
						},
						method: 'POST', // or 'PUT'
						body: JSON.stringify(data),
					}).then((response) => { 
						if (!response.ok) {
							return Promise.reject('Our servers are having issues! We couldn\'t send your feedback!');
						}
						response.json()
					}).then(() => {
						alert('Success!');
					}).catch((error) => {
						alert('Our servers are having issues! We couldn\'t send your feedback!', error);
					})
				}
				handleButtonClick={() => console.log("handleButtonClick")}
			/>
              </div>

            </div>
        )
    }

}
    
export default Chat
