import React from 'react';
import { Component } from 'react';
import Header from '../Header'
import {Redirect} from 'react-router-dom'
import FeedBack from '../feedback/feedback'

class About extends Component{
    constructor(props){
        super(props)
        const token = localStorage.getItem('token')
        let loggedIn = true
        if(token == null){
            loggedIn = false
        }
        this.state = { loggedIn }
    }
    render(){
        if(this.state.loggedIn === false){
            return <Redirect to = '/sign-in' />
        }
        return(
            <div>
                <Header/>
                <div class="content-wrapper">
            
                <div class="about-top position-relative"></div>
                <div class="banner about-banner">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-7 banner-content">
                    <h4 class="heading3">Infosys Online Learning Platform</h4>
                    <h6 class="heading3">Learners can interact with the voice-enabled ‘learning assistant’ for learning guidance. With an ever evolving range of curated content, it facilitates learning based on interest, skill set and function/role. Learning modules are a combination of instructor-led, assisted learning and self-learning sessions. With on-the-go access, Infosys Wingspan helps accelerate business and employee engagement. It aids talent in navigating their next by creating an environment for continuous learning to acquire the right and relevant skills.</h6>
                </div>
            </div>
        </div>
        <div class="timeline-top text-center d-none d-sm-block">
           <center> <img src="https://cdn.goodao.net/cyberwisdom/b02d5e9d.jpg" width="1000" height="360" alt="timeline top" /></center>
        </div>
    </div>
    <div class="about-lineart moon"></div>
    <div class="about-lineart circles"></div>
</div>    
                {/* <h1>Courses</h1> */}
                
                <FeedBack
				style={{zIndex:'1', position:'fixed', left:'2px!'}}
				position="left"
				numberOfStars={5}
				headerText="Feedback"
				bodyText="Give your valuable feedback"
				buttonText="FEEDBACK"
				handleClose={() => console.log("handleclose")}
				handleSubmit={(data) => 
					fetch('https://formspree.io/f/xqkwpwja', {
						headers: {
							Accept: 'application/json',
							'Content-Type': 'application/json'
						},
						method: 'POST', // or 'PUT'
						body: JSON.stringify(data),
					}).then((response) => { 
						if (!response.ok) {
							return Promise.reject('Our servers are having issues! We couldn\'t send your feedback!');
						}
						response.json()
					}).then(() => {
						alert('Success!');
					}).catch((error) => {
						alert('Our servers are having issues! We couldn\'t send your feedback!', error);
					})
				}
				handleButtonClick={() => console.log("handleButtonClick")}
			/>
            </div>
        )
    
    }
}
export default About;
