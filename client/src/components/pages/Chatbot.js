import React from 'react';
import Header from '../Header'
import { Component } from 'react';
import Chat from './Chat'
import {Redirect} from 'react-router-dom'

class Chatbot extends Component{

    constructor(props){
        super(props)
        const token = localStorage.getItem('token')

        let loggedIn = true
        if(token == null){
            loggedIn = false
        }

        this.state = { loggedIn }

    }

    render(){

        if(this.state.loggedIn === false){
            return <Redirect to = '/sign-in' />
        }

        return(
            <div>
                <Header/>
                <Chat/>

            </div>
        )
    }

}
    
export default Chatbot





  
