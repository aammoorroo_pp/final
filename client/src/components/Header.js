import React from 'react';
import { Link } from 'react-router-dom';

function refreshPage() {
    window.location.reload(false);
  }

const Header = () => (
        <nav>
            <div className="nav-wrapper" style={{background : '#0e9ca5'}}>
                <Link to={'#'} className="brand-logo">Infy Courses</Link>
                <ul id="nav-mobile" className="right hide-on-med-and-down">
                    <li><Link to={'/about'}>About</Link></li>
                    <li><Link to={'/chatbot'}>Chatbot</Link></li>
                    <li><Link to ={'/sign-in'} onClick={refreshPage}>Logout</Link></li>
                </ul>
            </div>
        </nav>
    )


export default Header;
