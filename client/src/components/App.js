import React, {Component} from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import SignUp from './signup.component'
import Login from './login.component'
import Welcome from './pages/Welcome';
import Chatbot from './pages/Chatbot';
import About from './pages/About';
import Logout from './pages/Logout';

      class App extends Component {
          constructor() {
            super();
            this.state = {
              name: "React"
            };
          }
        
          render() {
            
        
            return (
              <div>

                <BrowserRouter>
                <div >

                <Route exact path="/" component={Welcome} />
                <Route exact path="/sign-up" component={SignUp} />
                <Route exact path="/sign-in" component={Login} />
                <Route exact path="/chatbot" component={Chatbot} />
                <Route exact path="/about" component={About} />
                <Route exact path='/sign-in' component={Logout} />

                </div>
                </BrowserRouter>

              </div>
            );
          }
        }

export default App;

