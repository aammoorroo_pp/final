import axios from "axios";
import React, { Component } from "react";
import {Redirect} from 'react-router-dom'


const url = "http://localhost:8000/sign-up";

export default class SignUp extends Component {

    constructor(props){
        super(props);
      
        this.nameRef = React.createRef()
        this.pwdRef = React.createRef()
    }
    state={
        formVal:{
            firstName:'',
            lastName:'',
            emailId:'',
            password: ''
        },
        formErrMsg:{
            firstName:'',
            lastName:'',
            emailId:'',
            password: ''
        },
        formValid:{
            firstName:false,
            lastName:false,
            emailId:false,
            password:false,
            buttonActive:false
        },
        successmessage:'',
        errormessage:'',
        redirect : false
    }

    signup = async(event) => {
        event.preventDefault();
       try
       {
         let res=await axios.post(url,this.state.formVal);
         localStorage.setItem("token",res.data.token);
         localStorage.setItem("firstName",res.data.firstName);
         localStorage.setItem("lastName",res.data.lastName);
         localStorage.setItem("emailId",res.data.emailId);
       }catch(e)
       {
         if(e.status===404)
         {
           this.setState({errorMessage:e.response.data.message})
         } else{
           this.setState({errorMessage:"Could not signup"})
         }
       }

       this.setState({successmessage:' YOU HAVE SUCCESSFULLY REGISTERED ', redirect : true })
      }
    
    

    handleChange=(event) => {
        let { name, value } = event.target;
        // setting value
        let formValDum = this.state.formVal;
        formValDum[name] = value;
        this.setState( { formVal : formValDum } );
        this.validateField(name,value)
    }

    validateField = ( name, value ) =>{
        let formErrMsgDum = this.state.formErrMsg;
        let formValidDum=this.state.formValid
        let errMsg = ''; 
        let regEx = /[a-zA-Z]\d+@[a-z]+.\w/
        if(name === 'firstName'){
            if(value.length === 0){
                errMsg = 'field required'
            }else if( value.length < 5 ){
                errMsg = 'The first name must more than 5 characters'
            }
            formValidDum.firstName= errMsg ? false : true
            formErrMsgDum.firstName = errMsg;
        }else if(name==='lastName'){
            if(value.length === 0){
                errMsg = 'field required'
            }else if( value.length < 5 ){
                errMsg = 'The last name must be more than 5 characters'
            }
            formValidDum.lastName= errMsg ? false : true
            formErrMsgDum.lastName= errMsg;
        }
        else if(name==='emailId'){
            if(value.length === 0){
                errMsg = 'field required'
            }else if( !value.match(regEx)){
                errMsg = 'Provide valid email'
            }
            formValidDum.emailId= errMsg ? false : true
            formErrMsgDum.emailId= errMsg;
        }
    
        
        else if( name === 'password' ){
            if(value.length === 0){
                errMsg = 'field required'
            }else if( value.length < 8 ){
                errMsg = 'The password must be of minimum 8 characters'
            }
            formValidDum.password= errMsg ? false : true
            formErrMsgDum.password = errMsg;
        }
        formValidDum.buttonActive=formValidDum.firstName && formValidDum.lastName && formValidDum.password;
        this.setState( { formErrMsg : formErrMsgDum, formValid: formValidDum } );
    }


    render() {

        if(this.state.redirect){
            return <Redirect to='/sign-in' ></Redirect>
        }

        return(

            <div class="user">
            <header class="user__header">
            <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3219/logo.svg" alt="" />
            <h1 class="user__title">Welcome to InfyChatbot</h1>
            </header>
    
            <form class='form' onSubmit={this.signup}>

                <div class="form__group">
                    <input type="text" placeholder="First Name" name='firstName' id='firstName' class="form__input" value={this.state.formVal.firstName} onChange={this.handleChange}/>
                    <span style={{color:"red"}} name='uNameErr' className='text-danger'>{this.state.formErrMsg.firstName}</span>
                </div>

                <div class="form__group">
                    <input type="text" placeholder="Last Name" name='lastName' id='lastName' class="form__input" value={this.state.formVal.lastName} onChange={this.handleChange}/>
                    <span style={{color:"red"}} name='uNameErr' className='text-danger'>{this.state.formErrMsg.lastName}</span>
                </div>

                <div class="form__group">
                    <input type="email" placeholder="Email" class="form__input" name='emailId' id='emaiId' value={this.state.formVal.emailId} onChange={this.handleChange} />
                    <span style={{color:"red"}} name='emailErr' className='text-danger'>{this.state.formErrMsg.emailId}</span>
                </div>
                
                <div class="form__group">
                    <input type="password" placeholder="Password" class="form__input" name='password' id='password' value={this.state.formVal.password} onChange={this.handleChange}/>
                    <span style={{color:"red"}} name='pass' className='text-danger'>{this.state.formErrMsg.password}</span>
                </div>
                
                <button class="btn btn-primary" type="submit">Register</button>
                        <p className="forgot-password text-right" >
                            <h5 class="heading"> Already registered? </h5><a  href="/sign-in"> <h6 class="heading">Log in</h6></a>
                        </p>

                        <span style={{color:"green"}} name='successMessage' className='text-success'><h6 class="heading">{this.state.successmessage}</h6> </span>
            
            </form>
            </div>


        )

    }
}