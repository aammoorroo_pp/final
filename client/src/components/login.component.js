import axios from "axios";
import React, { Component } from "react";
import {Redirect} from "react-router-dom";

const url = "http://localhost:8000/sign-in";


 export default class Login extends Component {


    constructor(props){
        super(props);
        // const token = localStorage.getItem('token')
        // let loggedIn = true
        // if(token == null){
        //     loggedIn = false
        // }

        // this.state = { loggedIn }

        this.nameRef = React.createRef()
        this.pwdRef = React.createRef()

    }

    state={
        formVal:{
            emailId:'',
            password: ''
        },
        formErrMsg:{
            emailId:'',
            password: ''
        },
        formValid:{
            emailId:false,
            password:false,
            buttonActive:false
        },
        successmessage:'',
        errormessage:'',
        loggedIn : false

    }

    signin = async(event) => {
        event.preventDefault();

       try
       {
         let res=await axios.post(url,this.state.formVal);
         localStorage.setItem("token",res.data.token);
         localStorage.setItem("firstName",res.data.firstName);
         localStorage.setItem("lastName",res.data.lastName);
         localStorage.setItem("emailId",res.data.emailId);
         this.setState({successmessage:' YOUR ARE SUCCESSFULLY LOGGED IN ', loggedIn : true }) 
        //  localStorage.setItem('token', 'fuqbwhvuqehniqnrubhvuqniuqbu')
       }


    //    {
    //      if(e.status===404)
    //      {
    //        this.setState({errorMessage:e.response.data.message})
    //      } else{
    //        this.setState({errorMessage:"Could not signin"})
    //      }
    //    }
       catch(e)
       {
           if(e.status===409)
           {
               this.setState({errormessage : e.response.data.message})
           }
           else if(e.status===404)
           {
               this.setState({errormessage : e.response.data.message})
                //   console.log("404 error")
           }
           else if(e.status===401)
           {
               this.setState({errormessage : e.response.data.message})
           }
           else
           {
               this.setState({errormessage : e.response.data.message})
                //   console.log("else error")
           }
       }
    }

    
    

    handleChange=(event) => {
        let { name, value } = event.target;
        // setting value
        let formValDum = this.state.formVal;
        formValDum[name] = value;
        this.setState( { formVal : formValDum } );
        this.validateField(name,value)
    }

    validateField = ( name, value ) =>{
        let formErrMsgDum = this.state.formErrMsg;
        let formValidDum=this.state.formValid
        let errMsg = ''; 
        if(name === 'emailId'){
            if(value.length === 0){
                errMsg = 'field required'
            }else if( value.length < 5 ){
                errMsg = 'Invalid UserName'
            }
            formValidDum.emailId= errMsg ? false : true
            formErrMsgDum.emailId = errMsg;
        }else if( name === 'password' ){
            if(value.length === 0){
                errMsg = 'field required'
            }else if( value.length < 8 ){
                errMsg = 'Invalid Password'
            }
            formValidDum.password= errMsg ? false : true
            formErrMsgDum.password= errMsg;
        }
        formValidDum.buttonActive=formValidDum.emailId && formValidDum.password
        this.setState( { formErrMsg : formErrMsgDum, formValid: formValidDum } );
    }

    render() {

        // localStorage.removeItem('kommunicate')
        // sessionStorage.removeItem('kommunicate')
        // sessionStorage.removeItem('chatheaders')

        if(this.state.loggedIn){
            return <Redirect to='/chatbot' />
        }

        return (

            <div class="user">
            <header class="user__header">
            <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3219/logo.svg" alt="" />
            <h1 class="user__title">Log in</h1>
            </header>

            <form class="form" onSubmit={this.signin}>

                <div class="form__group">
                    <input type="email" placeholder="Email" class="form__input" name='emailId' id='emailId' value={this.state.formVal.emailId} onChange={this.handleChange} />
                    {/* <span style={{color:"red"}} name='emailErr' className='text-danger'>{this.state.formErrMsg.emailId}</span> */}
                </div>

                <div class="form__group">
                    <input type="password" placeholder="Password" class="form__input" name='password' id='password' value={this.state.formVal.password} onChange={this.handleChange}/>
                    {/* <span style={{color:"red"}} name='pass' className='text-danger'>{this.state.formErrMsg.password}</span> */}
                </div>

                <button type="submit" className="btn btn-primary" 
                disabled={!this.state.formValid.buttonActive} onClick={this.handle}>Sign in</button>
                <p className="forgot-password text-right">
                     <a href="/sign-up"><h5 class="heading">New User</h5></a>
                </p>

                <span style={{color:"green"}} name='successmessage' className='text-success'>{this.state.successmessage} </span>
                <span style={{color:"red"}} name='errormessage' className='text-danger'>{this.state.errormessage} </span>

                
            </form>

            </div>
            
        );
    
    }

}

