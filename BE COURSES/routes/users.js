const express = require('express');
const services= require('../service/users');

const router = express.Router();
const auth = require('../utilities/user')

router.post('/sign-up', async (req, res, next) => {
    try{
        let register= await services.register(req.body)
        res.json(register)
    } catch(err) {
        next(err)
    }
})

router.post('/sign-in', async (req, res, next) => {
    try{
        let login = await services.login(req.body)
        res.json(login)
    } catch(err) {
        next(err)
    }
})

module.exports=router;
