const User = require('../utilities/user')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const userDb= {}

userDb.findUser = async (emailId) => {
        let user = await User.findOne({"emailId":emailId})
        if(user) {
             return true 
                 }
        else {
            return false
         }
      }
 
userDb.register = async (userData) => {
        const newUser = new User({
              firstName :userData.firstName,
              lastName:userData.lastName,
              emailId:userData.emailId,
              password:userData.password
            //userProfileImage:"dummy.png",
        //userType:"user",
        //joinedDate: new Date(),
        
        })
       let salt = await bcrypt.genSalt(10)
       
       let hash = await bcrypt.hash(newUser.password,salt);
       newUser.password = hash
       let createNewUser = await newUser.save()
       let token = jwt.sign({id:createNewUser._id},"jwtSecretKey")
       let userDetails = {
              token,user: {
                   "firstName":createNewUser.userName, "lastName":createNewUser.userName , "emailId":createNewUser.emailId
               //      "userProfileImage":createNewUser.userProfileImage,"userType":createNewUser.userType,
               //     "userId":createNewUser._id
        }
        }
            return userDetails;
      }
   userDb.login = async(userData) => {
                     let user = await User.findOne({"emailId":userData.emailId})
                     let decryptPassword = await bcrypt.compare(userData.password,user.password)
                     if(!decryptPassword){
                            return null
                                }
             else{
            let token = jwt.sign({id: user._id},"jwtSecretKey")
                        let userDetails = {
              token,user: {
                "firstName": user.userName , "lastName": user.userName , "emailId": user.emailId
               //      "userProfileImage": user.userProfileImage,"userType": user.userType,
               //     "userId": user._id
        }
        }
            return userDetails;
            }
              }

module.exports = userDb
