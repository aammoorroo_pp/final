const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const router = require('./routes/users');

const cors = require("cors")
const app = express();

const url = "mongodb+srv://trent:x1ByxpJCKkSvzBtk@authtest.hcegy.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
const port= process.env.PORT || 8000;

app.use(cors())
app.use(bodyParser.json());


app.use('/', router);

mongoose.connect(url,{useNewUrlParser:true}).then((database=>{
    console.log("Connected to database");
})).catch((err=>{
    console.log("Failed to database connection");
}))


app.listen( port,()=> {
console.log("Server listening in port: ${port} ");
})


module.exports = app;
