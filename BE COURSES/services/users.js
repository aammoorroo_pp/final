const User = require('../utilities/user')
const userModel = require('../model/user')

let userService ={}

userService.register = async (userData) => {
      let existingUser = await userModel.findUser(userData.emailId)
    if(existingUser){
            let err = new Error("User already registered with this email Id")
            err.status=404
                        throw err;
            }
             else{
              let registerUser = await userModel.register(userData)
                      return registerUser
              }
        }

userService.login = async (userData) => {
                   let findUser = await userModel.findUser(userData.emailId)
               if(!findUser){
            let err = new Error("User doesnot exist.Please signup")
            err.status=404
                        throw err;
            }
                    let loginUser = await userModel.login(userData)
               if(loginUser == null){
            let err = new Error("Invalid Credentials")
            err.status=404
                        throw err;
            }
                   else{
                     return loginUser
                       }
     }
 
 module.exports = userService;
